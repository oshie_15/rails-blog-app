# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.2'

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem 'rails', '~> 7.0.8'

# Use postgresql as the database for Active Record
gem 'pg', '~> 1.1'

# Use the Puma web server [https://github.com/puma/puma]
gem 'puma', '~> 5.0'

# https://github.com/heartcombo/devise
gem 'devise', '~> 4.9', '>= 4.9.3', github: 'heartcombo/devise'
# JWT devise for API
# https://github.com/waiting-for-dev/devise-jwt
gem 'devise-jwt', '~> 0.11.0'
gem 'rack-cors', '~> 2.0', '>= 2.0.1'
# Use Active Storage variant
gem 'image_processing', '~> 1.12', '>= 1.12.2'
gem 'rubocop', '~> 1.57', '>= 1.57.2'

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
# gem "jbuilder"

# Use Redis adapter to run Action Cable in production
# gem "redis", "~> 4.0"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', require: false

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem "rack-cors"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem 'byebug'
  gem 'debug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~> 6.0', '>= 6.0.3'
  # Need for rspec
  gem 'rexml', '~> 3.2', '>= 3.2.6'
  gem 'spring-commands-rspec', '~> 1.0', '>= 1.0.4'
  # https://github.com/grosser/parallel_tests
  gem 'parallel_tests', '~> 4.3'
  # .env environment variable
  # https://github.com/bkeepers/dotenv
  gem 'dotenv-rails', '~> 2.8', '>= 2.8.1'
end

group :development do
  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  gem 'listen', '~> 3.8'
  gem 'spring'
  # https://github.com/ctran/annotate_models
  gem 'annotate', '~> 3.2'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 3.39', '>= 3.39.2'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'

  # Code coverage
  # https://github.com/colszowka/simplecov
  gem 'simplecov', require: false

  # Clear out database between runs
  # https://github.com/DatabaseCleaner/database_cleaner
  gem 'database_cleaner-active_record'
end
